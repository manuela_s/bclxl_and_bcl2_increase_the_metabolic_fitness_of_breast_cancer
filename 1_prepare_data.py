#!/usr/bin/env python3

import argparse
import pathlib

import ipywidgets
import pandas

import transcriptomics.map_ids_from_ensembl
import transcriptomics.util
import utils.data

BCL_GENES = ['BCL2', 'BCL2L1']


REFETCH_IDS_MAPPING = ipywidgets.Checkbox(
    value=False,
    description='fetch updated mapping from ensembl ids',
    indent=False,
)
REFETCH_IDS_MAPPING


def get_refetch_ids_mapping_flag():
    """Get flag value for refetch_ids_mapping.
    :return: bool.
    """
    try:
        get_ipython()
    except NameError:
        # Running in normal python environment. Parse command line flag
        parser = argparse.ArgumentParser()
        parser.add_argument('--refetch_ids_mapping', action='store_true')
        args = parser.parse_args()
        return args.refetch_ids_mapping
    else:
        return REFETCH_IDS_MAPPING.value


def _get_mcf7_rna():
    """Get MCF7 transcriptomic data.
    :return: pandas DataFrame, one row per sample (cell line/replica) and one column per ensembl id with normalized
             (rlog, DESeq2) values.
    """
    # Read in all raw count files and concatenate to a single DataFrame:
    files = pandas.Series(pathlib.Path('data', 'mcf7_clones', 'raw_counts').glob('*.txt'))
    samples = files.apply(lambda x: x.name).str.replace('.counts.txt', '')
    raw_counts = pandas.concat(
        [pandas.read_csv(f, sep='\t', header=None, names=['ensembl_id', 'count'], index_col=0) for f in files],
        keys=samples,
        names=['sample_id'])

    # Drop special counters:
    raw_counts.query('ensembl_id.str.startswith("ENSG")', inplace=True)

    raw_counts = raw_counts['count'].unstack('ensembl_id')

    # Sort the rows
    raw_counts.reset_index('sample_id', inplace=True)
    raw_counts['sample_id'] = pandas.Categorical(raw_counts.sample_id, [
        'MCF7_PSFFV_replica_1',
        'MCF7_PSFFV_replica_2',
        'MCF7_BCL2_replica_1',
        'MCF7_BCL2_replica_2',
        'MCF7_BCLXL_replica_1',
        'MCF7_BCLXL_replica_2',
    ])
    raw_counts.set_index('sample_id', inplace=True)
    raw_counts.sort_index(inplace=True)

    # Normalize with cell_line as design formula and rlog as method with DESeq2:
    raw_counts['cell_line'] = raw_counts.index.str.extract('(MCF7_(?:PSFFV|BCL2|BCLXL))', expand=False)
    raw_counts.set_index('cell_line', append=True, inplace=True)
    normalized = transcriptomics.util.prepare_rna(
        raw_counts,
        min_count_fraction=0.2,
        formula_str='~cell_line',
        normalization_method='rlog')
    normalized.reset_index('cell_line', drop=True, inplace=True)

    return normalized


def prepare_mcf7_data(genes_to_include, refetch_ids_mapping):
    """Prepare transcriptomic data for the MCF7 clones restricted to genes of interest.
    :param genes_to_include: list of gene symbols, genes related to respiratory chain (as returned by
                             utils.data.get_relevant_genes());
    :param refetch_ids_mapping: boolean, if enabled re-fetch mapping between ensembl identifiers and other gene
                                identifiers, otherwise use stored mapping;
    Export csv file, one row per sample (cell line and replica) and columns including genes listed in 'genes_to_include'.
    """
    print('Preparing MCF7 dataset')
    data = _get_mcf7_rna()

    # Export normalized transcriptomic data (as uploaded in GSE158808):
    data.to_csv(pathlib.Path('data', 'mcf7_clones', 'deseq2_rlog_normalized.csv'))

    if refetch_ids_mapping:
        # Fetch updated gene mapping from mygene:
        mapping = transcriptomics.map_ids_from_ensembl.create_gene_mapping(data.columns)
        mapping_fn = pathlib.Path('data', 'mcf7_clones', 'deseq2_rlog_normalized_refetched_mapping.csv')
        mapping.to_csv(mapping_fn)
    else:
        # Use stored gene mapping as used for the analysis in the manuscript:
        mapping_fn = pathlib.Path('data', 'mcf7_clones', 'deseq2_rlog_normalized_mapping.csv')

    # Re-map ensembl identifiers to symbol
    data = transcriptomics.util.remap_rna_ids(data, mapping_fn, 'symbol').rename_axis(columns='gene')

    # Split sample_id into cell_line and replica info
    data['cell_line'] = data.index.get_level_values('sample_id').str.extract('(MCF7_(?:PSFFV|BCL2|BCLXL))', expand=False)
    data['replica'] = data.index.get_level_values('sample_id').str.extract('(replica_(?:1|2))', expand=False)
    data.reset_index(inplace=True)
    data.set_index(['cell_line', 'replica'], inplace=True)

    # Include only genes involved in the respiratory chain complexes
    data = data[data.columns[data.columns.isin(genes_to_include)]]

    # Export data for downstream analysis
    data.to_csv(pathlib.Path('data', 'mcf7_clones', 'mcf7.csv'))
    print('Exported prepared MCF7 dataset to data/mcf7_clones/mcf7.csv')


def _get_ccle_metadata():
    """Read in metadata from the CCLE cell line collection (Barretina et al., Nature, 2012; PMID: 22460905).
    :return: pandas DataFrame, one row per cell line and indices including 'tcga_code' and 'sample'.
    """
    metadata = pandas.read_csv(pathlib.Path('download', 'ccle', 'Cell_lines_annotations_20181226.txt'),
                               sep='\t',
                               index_col=['tcga_code', 'type', 'type_refined', 'CCLE_ID'])
    metadata['sample'] = metadata.Name.str.replace('-|\.', ' ').str.replace(' ', '').values
    metadata.set_index('sample', append=True, inplace=True)

    return metadata


def _get_ccle_rna():
    """Read in transcriptomic data from the CCLE cell line collection (Barretina et al., Nature, 2012; PMID: 22460905).
    :return: pandas DataFrame, one row per cell line and columns with genes.
    """
    rna = pandas.read_csv(pathlib.Path('download', 'ccle', 'CCLE_RNAseq_rsem_genes_tpm_20180929.txt.gz'),
                          sep='\t',
                          index_col=['gene_id'])
    rna.drop(columns=['transcript_ids'], inplace=True)
    rna.columns = [x[0] for x in rna.columns.str.split('_')]
    rna = rna.T
    rna.index.names = ['sample']

    # Remove trailing in ENSG id
    rna.columns = rna.columns.str[:15]

    return rna


def prepare_ccle_data(genes_to_include, refetch_ids_mapping=False):
    """Prepare transcriptomic data for the CCLE cell line collection (Barretina et al., Nature, 2012; PMID: 22460905)
    restricted to breast cancer cell lines and genes of interest.
    :param genes_to_include: list of gene symbols, BCL genes and genes related to respiratory chain (as returned by
                             utils.data.get_relevant_genes())
    Export csv file, one row per cell line and columns including genes listed in 'genes_to_include'.
    """
    print('Preparing CCLE dataset')
    metadata = _get_ccle_metadata().query('tcga_code=="BRCA"')
    rna = _get_ccle_rna()

    if refetch_ids_mapping:
        # Fetch updated gene mapping from mygene:
        mapping = transcriptomics.map_ids_from_ensembl.create_gene_mapping(rna.columns)
        mapping_fn = pathlib.Path('data', 'ccle', 'ccle_refetched_mapping.csv')
        mapping.to_csv(mapping_fn)
    else:
        # Use stored gene mapping as used for the analysis in the manuscript:
        mapping_fn = pathlib.Path('data', 'ccle', 'ccle_mapping.csv')

    # Map ensembl ids to symbols
    rna = transcriptomics.util.remap_rna_ids(rna, mapping_fn, 'symbol')

    data = metadata.reset_index().set_index('sample')[[]].join(rna).dropna(). \
        rename_axis(index={'sample': 'cell_line'}, columns={None: 'gene'})

    # Include only genes involved in the respiratory chain complexes
    data = data[data.columns[data.columns.isin(genes_to_include)]]

    # Export data for downstream analysis
    data.to_csv(pathlib.Path('data', 'ccle', 'ccle.csv'))
    print('Exported prepared CCLE dataset to data/ccle/ccle.csv')


def _get_pdx_metadata():
    """Read in metadata from patient-derived xenografts (Gao et al., Nature Medicine, 2015; PMID: 26479923).
    :return: pandas DataFrame, one row per PDX model per treatment and columns: 'tumor_type' and 'treatment'.
    """
    metadata = pandas.read_excel(
        pathlib.Path('download', 'pdxs_gao', '41591_2015_BFnm3954_MOESM10_ESM.xlsx'),
        sheet_name='PCT raw data',
        usecols=['Model', 'Tumor Type', 'Treatment'])

    metadata.columns = metadata.columns.str.lower().str.replace(' ', '_')
    metadata.rename(columns={'model': 'pdx'}, inplace=True)

    metadata.drop_duplicates(inplace=True)
    metadata.set_index('pdx', inplace=True)

    return metadata


def _get_pdx_rna():
    """Read in transcriptomic data from patient-derived xenografts (Gao et al., Nature Medicine, 2015; PMID: 26479923).
    :return: pandas DataFrame, one row per PDX model and columns with genes (symbols).
    """
    rna = pandas.read_excel(
        pathlib.Path('download', 'pdxs_gao', '41591_2015_BFnm3954_MOESM10_ESM.xlsx'),
        sheet_name='RNAseq_fpkm',
        index_col='Sample'). \
        T.rename_axis(index={None: 'pdx'}, columns={'Sample': 'gene'})

    return rna


def prepare_pdx_data(genes_to_include):
    """Prepare transcriptomic data from patient-derived xenografts (PDXs) for untreated PDXs from breast cancer patients
    (Gao et al., Nature Medicine, 2015; PMID: 26479923) and restricted to genes of interest.
    :param genes_to_include: list of gene symbols, BCL genes and genes related to respiratory chain (as returned by
                             utils.data.get_relevant_genes())
    Export csv file, one row per PDX and columns including genes listed in 'genes_to_include'.
    """
    print('Preparing PDX dataset')
    metadata = _get_pdx_metadata()
    pdxs_to_keep = metadata.query('tumor_type == "BRCA" and treatment == "untreated"').index

    rna = _get_pdx_rna()
    data = rna[rna.index.get_level_values('pdx').isin(pdxs_to_keep)]

    # Re-map selected gene names for Complex V to match spelling used in other datasets:
    data = data.rename(columns={
        'ATP5F1': 'ATP5PB',
        'C14orf2': 'ATP5MPL',
        'ATP5L': 'ATP5MG',
        'ATP5J2': 'ATP5MF',
        'ATP5I': 'ATP5ME',
        'ATP5G2': 'ATP5MC2',
        'ATP5E': 'ATP5F1E',
        'ATP5D': 'ATP5F1D',
        'ATP5C1': 'ATP5F1C',
    })

    # Include only genes involved in the respiratory chain complexes
    data = data[data.columns[data.columns.isin(genes_to_include)]]

    # Export data for downstream analysis
    data.to_csv(pathlib.Path('data', 'pdxs_gao', 'pdxs_gao.csv'))
    print('Exported prepared PDX dataset to data/pdxs_gao/pdxs_gao.csv')


def prepare_metabric_data(genes_to_include):
    """Prepare transcriptomic data from the METABRIC patients cohort (Curtis et al., Nature, 2012; PMID: 22522925)
    restricted to genes of interest.
    :param genes_to_include: list of gene symbols, BCL genes and genes related to respiratory chain (as returned by
                             utils.data.get_relevant_genes())
    Export csv file, one row per patient and columns including genes listed in 'genes_to_include'.
    """
    print('Preparing METABRIC dataset')
    data = pandas.read_table(
        pathlib.Path('download', 'metabric', 'data_mRNA_median_Zscores.txt'),
        index_col=[0, 1])

    # Aggregate by mean multiple Entrez_Gene_Id mapping to the same Hugo_Symbol
    data = data.groupby('Hugo_Symbol').mean()

    data = data.T
    data.rename_axis(index=['patient_id'], columns='gene', inplace=True)

    # Include only genes involved in the respiratory chain complexes
    data = data[data.columns[data.columns.isin(genes_to_include)]]

    # Export data for downstream analysis
    data.to_csv(pathlib.Path('data', 'metabric', 'metabric.csv'))
    print('Exported prepared METABRIC dataset to data/metabric/metabric.csv')


if __name__ == '__main__':
    refetch_ids_mapping = get_refetch_ids_mapping_flag()

    pathlib.Path('data', 'ccle').mkdir(parents=True, exist_ok=True)
    pathlib.Path('data', 'pdxs_gao').mkdir(parents=True, exist_ok=True)
    pathlib.Path('data', 'metabric').mkdir(parents=True, exist_ok=True)

    # Identify genes involved in the respiratory chain complexes
    genes = utils.data.get_relevant_genes()
    # Include in downstream analysis both genes related to BCL family and respiratory chain complexes
    genes_to_include = BCL_GENES + genes.index.to_list()

    prepare_ccle_data(genes_to_include, refetch_ids_mapping=refetch_ids_mapping)
    prepare_metabric_data(genes_to_include)
    prepare_mcf7_data(genes_to_include, refetch_ids_mapping=refetch_ids_mapping)
    prepare_pdx_data(genes_to_include)

    print('Data prepared successfully')
