#!/usr/bin/env python3

import pathlib
import subprocess
import sys
import urllib.request

import tqdm


class TqdmUpTo(tqdm.tqdm):
    """From https://github.com/tqdm/tqdm/blob/master/examples/tqdm_wget.py.
    Provides `update_to(n)` which uses `tqdm.update(delta_n)`.
    Inspired by [twine#242](https://github.com/pypa/twine/pull/242),
    [here](https://github.com/pypa/twine/commit/42e55e06).
    """

    def update_to(self, b=1, bsize=1, tsize=None):
        """
        b  : int, optional
            Number of blocks transferred so far [default: 1].
        bsize  : int, optional
            Size of each block (in tqdm units) [default: 1].
        tsize  : int, optional
            Total size (in tqdm units). If [default: None] remains unchanged.
        """
        if tsize is not None:
            self.total = tsize
        return self.update(b * bsize - self.n)  # also sets self.n = b * bsize


def download_helper(url, destination, description):
    """Helper to download url with progress bar.
    :param url: string, url to download from;
    :param destination: pathlib.Path, destination path;
    :param description: string, description of the download.
    """
    with TqdmUpTo(unit='B', unit_scale=True, unit_divisor=1024, miniters=1, desc=description) as t:
        urllib.request.urlretrieve(url, destination, reporthook=t.update_to)


def download_ccle_data():
    """Download cell line collection dataset (CCLE, Barretina et al., Nature, 2012; PMID: 22460905).
    Dataset is downloaded to the 'download/ccle' directory.
    """
    pathlib.Path('download', 'ccle').mkdir(parents=True, exist_ok=True)

    download_helper('https://data.broadinstitute.org/ccle/Cell_lines_annotations_20181226.txt',
                    pathlib.Path('download', 'ccle', 'Cell_lines_annotations_20181226.txt'),
                    'Downloading metadata for the CCLE dataset: Cell_lines_annotations_20181226.txt')

    download_helper('https://data.broadinstitute.org/ccle/CCLE_RNAseq_rsem_genes_tpm_20180929.txt.gz',
                    pathlib.Path('download', 'ccle', 'CCLE_RNAseq_rsem_genes_tpm_20180929.txt.gz'),
                    'Downloading transcriptomics data for the CCLE dataset: CCLE_RNAseq_rsem_genes_tpm_20180929.txt.gz')


def download_pdx_data():
    """Download dataset for patient-derived xenografts (PDXs, Gao et al., Nature Medicine, 2015; PMID: 26479923).
    Dataset is downloaded to the 'download/pdxs_gao' directory.
    """
    pathlib.Path('download', 'pdxs_gao').mkdir(parents=True, exist_ok=True)

    download_helper('https://static-content.springer.com/esm/art%3A10.1038%2Fnm.3954/MediaObjects/41591_2015_BFnm3954_MOESM10_ESM.xlsx',
                    pathlib.Path('download', 'pdxs_gao', '41591_2015_BFnm3954_MOESM10_ESM.xlsx'),
                    'Downloading PDX dataset: 41591_2015_BFnm3954_MOESM10_ESM.xlsx')


def download_metabric_data():
    """Download dataset for the METABRIC study (Curtis et al., Nature, 2012; PMID: 22522925).
    Dataset is downloaded to the 'download/metabric' directory.
    """
    pathlib.Path('download', 'metabric').mkdir(parents=True, exist_ok=True)

    download_helper(
        'https://media.githubusercontent.com/media/cBioPortal/datahub/master/public/brca_metabric/data_mRNA_median_Zscores.txt',
        pathlib.Path('download', 'metabric', 'data_mRNA_median_Zscores.txt'),
        'Downloading METABRIC dataset: data_mRNA_median_Zscores.txt')


def check_download_checksums():
    """Check that checksums of downloaded files match expectations."""
    # Capture output for use in jupyter notebook
    completed_process = subprocess.run(['md5sum', '-c', 'downloads.md5'],
                                       cwd='download',
                                       stdout=subprocess.PIPE,
                                       stderr=subprocess.STDOUT)
    if completed_process.returncode:
        sys.stderr.write(completed_process.stdout.decode())
        exit(completed_process.returncode)
    else:
        sys.stdout.write(completed_process.stdout.decode())
        print('Downloads completed successfully')


if __name__ == '__main__':
    download_ccle_data()
    download_pdx_data()
    download_metabric_data()
    check_download_checksums()
