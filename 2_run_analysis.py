#!/usr/bin/env python3

import pathlib

import matplotlib.pyplot

import utils.data
import utils.figure
import utils.stats


if __name__ == '__main__':
    matplotlib.rcParams['pdf.fonttype'] = 42
    matplotlib.pyplot.rcParams.update({'font.size': 6})

    pathlib.Path('outputs').mkdir(parents=True, exist_ok=True)

    # Load in-house transcriptomic data for the MCF7 clones
    mcf7 = utils.data.get_mcf7_data()

    # Supplementary Table 1
    gene_anova = utils.stats.gene_expression_by_cell_line_stats(mcf7)

    # Restrict visualization and validation analysis in other datasets to genes found to be altered in the MCF7 clones
    significant_genes = gene_anova.query("overall_anova_pvalue < 0.05").index.get_level_values('gene')

    # Compute association between genes of the BCL family and restricted gene list
    corr_data = utils.stats.compute_correlations_stats_between_bcl_family_and_genes(significant_genes)

    # Figure 8
    utils.figure.plot_association_between_bcl_family_and_genes(
        mcf7.copy().query('gene.isin(@significant_genes)'),  # subset to genes that differ by cell_line
        corr_data[['is_significant', 'R']])

    # Supplementary Table 2
    utils.stats.report_correlation_stats_between_bcl_family_and_genes(corr_data)
