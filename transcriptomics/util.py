import pandas
import rpy2.robjects
import rpy2.robjects.packages
import rpy2.robjects.pandas2ri


def normalize_rna(data, formula_str, normalization_method):
    """Normalize RNASeq data with DESeq2 from R using either varianceStabilizatingTransformation or rlogTransformation.
    :param data: pandas DataFrame with one sample per row and one gene (ENSEMBL identifier) per column;
    :param formula_str: str, R-style formula string for design;
    :param normalization_method: string, normalization method, either 'vsd' or 'rlog' for
           varianceStabilizatingTransformation or rlogTransformation, respectively;
    :return: pandas DataFrame with data, with same shape, index and columns as 'data', with normalized values.
    """
    DESeq2 = rpy2.robjects.packages.importr('DESeq2')
    SummarizedExperiment = rpy2.robjects.packages.importr('SummarizedExperiment')
    formula = rpy2.robjects.Formula(formula_str)

    count_data = data.T
    col_data = data.index.to_frame(False)

    # Replace count_data column names and col_data index with names compatible with R dataframe index and colum names.
    # (Can not use integer/rangeindex or MultiIndex)
    count_data.columns = col_data.index = ['x_{}'.format(i) for i in range(data.shape[0])]

    with rpy2.robjects.conversion.localconverter(rpy2.robjects.default_converter + rpy2.robjects.pandas2ri.converter):
        deseq_data_set = DESeq2.DESeqDataSetFromMatrix(count_data, col_data, design=formula)

    if normalization_method == 'vsd':
        transform = DESeq2.varianceStabilizingTransformation(deseq_data_set, blind=False)
    else:
        transform = DESeq2.rlogTransformation(deseq_data_set, blind=False)

    with rpy2.robjects.conversion.localconverter(rpy2.robjects.default_converter + rpy2.robjects.pandas2ri.converter):
        normalized = rpy2.robjects.conversion.rpy2py(SummarizedExperiment.assay(transform)).T

    return pandas.DataFrame(normalized, index=data.index, columns=data.columns)


def prepare_rna(data, min_count_fraction=None, formula_str='~1', normalization_method='vsd'):
    """Prepare RNA data.
    :param data: pandas DataFrame with one sample per row and one gene per column;
    :param min_count_fraction: float (range 0, 1), if set, drop genes where less than min_count_fraction of samples have
           non-zero counts;
    :param formula_str: str, R-style formula string for design for normalization with DESeq2;
    :param normalization_method: string, normalization method for DESeq2, either 'vsd' or 'rlog';
    :return: pandas DataFrame with data, one row per sample and one column per ensemble_id (sorted in decreasing order
             of variance).
    """
    if min_count_fraction:
        data = data.loc[:, data.astype(bool).mean() > min_count_fraction]

    data = normalize_rna(data, formula_str, normalization_method)
    data = data[data.var().sort_values(ascending=False).index]

    return data


def remap_rna_ids(data, mapping_fn, output_id):
    """Rename column-names in data from one type of rna identifier to another (output_id).
    Drop columns not mapping to any output_id and keeps data column with highest variance if multiple genes map to
    the same output_id.
    :param data: pandas DataFrame with gene IDs as column names and samples as rows;
    :param mapping_fn: filename for csv file that maps data column names to other RNA ids (see map_ids_from_ensembl.py);
    :param output_id: string, output_id type, either 'ensembl_id', 'symbol' or 'entrezgene';
    :return: pandas DataFrame, with same index as 'data' and columns remapped to 'output_id'.
    """
    m = pandas.read_csv(mapping_fn, dtype={'entrezgene': str, 'query': str})
    m.set_index(['query'], inplace=True)

    if 'notfound' in m.columns:
        m.drop(index=m.index[m.notfound == True], inplace=True)

    # Drop entries where query did not match to any output id
    m.dropna(subset=[output_id], inplace=True)

    # Keep highest entrezgene when a single query maps to multiple output_id.
    m.sort_values(by='entrezgene', inplace=True)
    m = m[~m.index.duplicated(keep='last')]

    # If multiple gene ids map to the same output_id, keep the gene id corresponding to the highest variance.
    variance = data.apply(pandas.Series.var).to_frame('variance').join(m[output_id], how='inner')
    gene2output_id = variance.sort_values(by='variance', ascending=False)[output_id].drop_duplicates(keep='first')

    # Drop columns that are missing output_id mapping
    data = data[gene2output_id.index]

    # Rename columns to be named by output_id
    data.columns = data.columns.map(gene2output_id)
    data.columns.name = output_id

    return data
