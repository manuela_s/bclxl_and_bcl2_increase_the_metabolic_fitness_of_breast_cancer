import mygene


def create_gene_mapping(rna_ids):
    """Create a DataFrame that maps RNA identifiers ('ensemblid', 'entrezegene' or 'symbol') to other gene ids.
    :param rna_ids: sequence of strings, RNA identifiers ('ensemblid', 'entrezegene' or 'symbol');
    :return: pandas DataFrame, one row per queried RNA identifier and columns including 'ensembl_id', 'entrezegene' and
             'symbol'.
    """
    mg = mygene.MyGeneInfo()
    m = mg.querymany(rna_ids,
                     scopes=['ensembl.gene', 'entrezgene', 'symbol', 'alias', 'name', 'other_names'],
                     fields=['symbol', 'entrezgene', 'ensembl.gene', 'name', 'alias', 'description'],
                     species='human',
                     as_dataframe=True,
                     dotfield=True)
    m.rename(columns={'ensembl.gene': 'ensembl_id'}, inplace=True)

    return m
