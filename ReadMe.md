[![pythonv3.8](https://img.shields.io/badge/python-v3.8-blue)](https://www.python.org/)
[![License](https://img.shields.io/badge/License-BSD%202--Clause-blue.svg)](https://opensource.org/licenses/bsd-license.php)
[![ZENODO](https://zenodo.org/badge/DOI/10.5281/zenodo.4058036.svg)](https://doi.org/10.5281/zenodo.4058036)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fmanuela_s%40bitbucket.org%2Fmanuela_s%2Fbclxl_and_bcl2_increase_the_metabolic_fitness_of_breast_cancer.git/master)


# BCL(X)L and BCL2 increase the metabolic fitness of breast cancer cells: a single cell imaging study

Datasets and source-code underlying the gene expression analysis for the respiratory chain complexes included in the
manuscript *"BCL(X)L and BCL2 increase the metabolic fitness of breast cancer cells: a single cell imaging study"*
authored by Federico Lucantoni, Manuela Salvucci, Heiko Dussmann, Andreas U Lindner, Diether Lambrechts and Jochen H. M.
Prehn (submitted 2020).

Transcriptomic data (FASTQ files, raw counts and rlog normalized expression) for the MCF7 cell lines (parental
MCF7-PSFFV and BCL2- and BCL-xL overexpressing clones, MCF7-BCL2 and MCF7-BCLXL, in duplicates) have been deposited at
[Gene Expression Omnibus](https://www.ncbi.nlm.nih.gov/geo/) and are publicly available with accession number
[GSE158808](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE158808).


## Requirements

Analysis was performed with python 3.8 and R 3.6.3 in Ubuntu 20.04.
The analysis code depends on ubuntu packages listed in [apt.txt](binder/apt.txt) and python listed in
[requirements.txt](binder/requirements.txt).

Key libraries, corresponding programming language and usage in this study are listed in the table below.

|Language           | Description                               | Package                                                                                                                                                                     |
| :---------------- | :--------------------------------------   | :-------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Python            | Data ingestion, cleaning and wrangling    | [*pandas*](https://doi.org/10.5281/zenodo.3509134), [*numpy*](https://dx.doi.org/10.1109/MCSE.2011.37), [*sklearn*](http://jmlr.csail.mit.edu/papers/v12/pedregosa11a.html) |
| Python            | Data visualization                        | [*matplotlib*](https://doi.org/10.5281/zenodo.3714460), [*seaborn*](https://doi.org/10.5281/zenodo.3767070)                                                                 |
| Python            | Statistical analysis                      | [*statsmodels*](https://github.com/statsmodels/statsmodels), [*pingouin*](https://joss.theoj.org/papers/10.21105/joss.01026)                                                |
| Python            | Utilities                                 | [*tqdm*](https://doi.org/10.5281/zenodo.595120), [*rpy2*](https://github.com/rpy2/rpy2), [*mygene*](http://mygene.info/citation/)                                           |
| R                 | Statistical analysis                      | [*DESeq2*](https://genomebiology.biomedcentral.com/articles/10.1186/s13059-014-0550-8)                                                                                      |

A [Dockerfile](binder/Dockerfile) is available for building a docker container with all requirements. The container
can be built and run with:
```bash
docker build -f binder/Dockerfile --tag transcriptomic_analysis_bcl_family:1.0 .
docker run --publish 8888:8888 --tty --interactive transcriptomic_analysis_bcl_family:1.0
```

Alternatively, the container is available in [Binder](https://mybinder.org/v2/git/https%3A%2F%2Fmanuela_s%40bitbucket.org%2Fmanuela_s%2Fbclxl_and_bcl2_increase_the_metabolic_fitness_of_breast_cancer.git/master).


## Usage

To reproduce the results of the analysis, please run the following scripts sequentially:

| Script                                   | Description                                                                                                                                                                                                                                                                                                                                                                          |
| :--------------------------------------- | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [0_download_data.py](0_download_data.py) | Download gene expression and metadata for datasets derived from cell lines (in-house MCF7 clones and Cancer Cell Line Encyclopedia (CCLE) collection, Barretina et al., Nature, 2012), patient-derived xenografts (Gao et al., Nat. Med., 2015) and patients from the 'Molecular Taxonomy of Breast Cancer International Consortium' (METABRIC) study (Curtis et al., Nature, 2012). |
| [1_prepare_data.py](1_prepare_data.py)   | Prepare datasets for downstream analysis. Use --refetch_ids_mapping flag to fetch updated mapping from ensembl identifiers to gene symbols. Defaults to use stored mappings to reproduce analysis presented in the manuscript.                                                                                                                                                       |
| [2_run_analysis.py](2_run_analysis.py)   | Investigate association between expression of genes involved in the respiratory chain and BCL genes and generate figure and tables with statistical analysis                                                                                                                                                                                                                         |

```bash
./0_download_data.py
./1_prepare_data.py
./2_run_analysis.py
```


## Citation

Please cite https://doi.org/10.5281/zenodo.4058036 and the corresponding paper, if using these datasets and/or source code.


## Contact information

- Prof. Jochen H. M. Prehn (JPrehn@rcsi.ie);
- Manuela Salvucci (manuelasalvucci@rcsi.ie);


## Funding

This work was supported by generous funding from the Irish Cancer Society Collaborative Cancer Research Centre
BREAST-PREDICT (CCRC13GAL) and from Science Foundation Ireland and the Health Research Board (13/IA/1881; 16/US/3301)


## Acknowledgement

The results shown here are in part based upon data generated by the [CCLE](https://portals.broadinstitute.org/ccle)
network and from published data for patient-derived xenografts
[(Gao et al., Nature Medicine, 2015)](https://pubmed.ncbi.nlm.nih.gov/26479923/) and patients
[METABRIC, Curtis et al., Nature, 2012](https://pubmed.ncbi.nlm.nih.gov/22522925/).
