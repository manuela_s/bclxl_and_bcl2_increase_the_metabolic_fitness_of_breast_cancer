import pathlib

import numpy
import pandas
import pingouin
import statsmodels.formula.api
import statsmodels.stats.anova
import statsmodels.stats.multicomp

import py_utils.format_pvalue
import utils.data

CELL_LINE_COLORS = ['#bdbdbd', '#1f78b4', '#a6cee3']
CELL_LINE_ORDER = ['MCF7_PSFFV', 'MCF7_BCL2', 'MCF7_BCLXL']
BCL_GENES = ['BCL2', 'BCL2L1']


def _anova_p_helper(df):
    """Compute ANOVA p-value for association between 'expression' and 'cell_line'.
    :param df: pandas DataFrame, one row per observation with columns 'expression' and 'cell_line';
    :return: float, p-value.
    """
    formula = 'expression ~ cell_line'
    model = statsmodels.formula.api.ols(formula, df).fit()
    aov_t = statsmodels.stats.anova.anova_lm(model, typ=1)

    return aov_t.loc['cell_line', 'PR(>F)']


def _tukey_posthoc_tests_helper(df):
    """Compute pairwise group comparisons with Tukey PostHoc tests for 'expression' grouped by 'cell_line'.
    :param df: pandas DataFrame, one row per observation with columns 'expression' and 'cell_line';
    :return: pandas DataFrame, one row per pair of 'cell_line' groups and columns: 'group1', 'group2', 'meandiff',
             'p-adj', 'lower', 'upper', 'reject'.
    """
    mc = statsmodels.stats.multicomp.MultiComparison(df.expression, df.cell_line, CELL_LINE_ORDER).tukeyhsd(alpha=0.05)

    return pandas.DataFrame(mc.summary().data[1:], columns=mc.summary().data[0])


def gene_expression_by_cell_line_stats(df):
    """Compute statistics for association between gene expression and cell_line.
    :param df: pandas DataFrame, one row per sample per gene with indices including "cell_line" and "gene_group" and one
               column "expression";
    :return: pandas Dataframe, one row per gene with column "overall_anove_pvalue".
    Export results to a styled excel spreadsheet.
    """
    df = df.copy().reset_index()

    # Compute statistics
    groups = df.groupby(['gene_group', 'gene'])
    anova = groups.apply(_anova_p_helper).to_frame('overall_anova_pvalue')
    posthoc = groups.apply(_tukey_posthoc_tests_helper).reset_index([None], drop=True)
    stats = pandas.concat([anova, posthoc], axis=0, sort=False).sort_index(kind='mergesort')
    stats.reset_index(inplace=True)
    stats = stats[['gene_group', 'gene', 'group1', 'group2', 'meandiff', 'lower', 'upper', 'p-adj',
                   'overall_anova_pvalue']]

    # Format table for export
    stats['gene_group'] = stats.gene_group.astype(str).where(~(stats.gene_group == stats.gene_group.shift(1)), '')
    stats['gene'] = stats.gene.astype(str).where(~(stats.gene == stats.gene.shift(1)), '')
    stats['meandiff'] = stats.meandiff.round(1)
    stats['lower'] = stats.lower.round(1)
    stats['upper'] = stats.upper.round(1)
    stats['p-adj'] = stats['p-adj'].apply(lambda x: '' if numpy.isnan(x) else py_utils.format_pvalue.format_pvalue(x))
    stats['overall_anova_pvalue'] = stats.overall_anova_pvalue.apply(
        lambda x: '' if numpy.isnan(x) else py_utils.format_pvalue.format_pvalue(x))

    stats.rename(columns={
        'gene_group': 'Gene group',
        'gene': 'Gene',
        'group1': 'Group 1',
        'group2': 'Group 2',
        'meandiff': 'Mean difference',
        'lower': '2.5% CI',
        'upper': '97.5% CI',
        'p-adj': 'PostHoc Tukey P-value',
        'overall_anova_pvalue': 'Overall ANOVA P-value'
    }, inplace=True)

    def color_by_cell_line(data):
        if data == 'MCF7_PSFFV':
            color = CELL_LINE_COLORS[0]
        elif data == 'MCF7_BCL2':
            color = CELL_LINE_COLORS[1]
        elif data == 'MCF7_BCLXL':
            color = CELL_LINE_COLORS[2]
        else:
            return ''
        return 'background-color: {}'.format(color)

    # Apply style and export to excel spreadsheet
    stats.style. \
        applymap(color_by_cell_line, subset=['Group 1', 'Group 2']). \
        apply(lambda x: ['border-top-style: solid' if x['Overall ANOVA P-value'] else ''] * len(x), axis=1). \
        apply(lambda x: ['font-weight: bold' if x['Overall ANOVA P-value'] else ''] * len(x), axis=1). \
        set_properties(**{'text-align': 'left'}). \
        set_properties(subset=['Mean difference', '2.5% CI', '97.5% CI'], **{'text-align': 'right'}). \
        to_excel(pathlib.Path('outputs', 'supplementary_table_1.xlsx'), index=False)

    print('Results from statistical analysis saved in outputs/supplementary_table_1.xlsx')

    return anova


def compute_correlations_stats_between_bcl_family_and_genes(significant_genes):
    """Compute correlation statistics between genes of the BCL family and genes found to be altered in MCF7 clones.
    :param significant_genes: list of gene symbols, genes related to respiratory chain with differential expression by
           BCL family memebers in MCF7 clones;
    :return: pandas DataFrame, one row per validation 'dataset' ('ccle', 'pdx', 'metabric'), 'bcl_gene' (BCL_GENES) and
            'gene' (significant_genes) and columns:
             - 'is_significant': boolean, whether association is statistically significant (0.05 cut-off);
             - 'R': float, Spearman correlation (R);
             - 'p-value': float, p-value.
    """
    data = pandas.concat([utils.data.get_ccle_data(), utils.data.get_pdx_data(), utils.data.get_metabric_data()],
                         names=['dataset'],
                         keys=['ccle', 'pdx', 'metabric'])[BCL_GENES + significant_genes.to_list()]

    def correlation_helper(data):
        return pingouin.pairwise_corr(data, [BCL_GENES, significant_genes], method='spearman')

    data_corr = data.groupby('dataset').apply(correlation_helper).rename_axis(index={None: 'tmp'}). \
        reset_index('tmp', drop=True)

    data_corr.rename(columns={'X': 'bcl_gene', 'Y': 'gene', 'r': 'R', 'p-unc': 'p_value'}, inplace=True)
    data_corr['bcl_gene'] = pandas.Categorical(data_corr.bcl_gene, BCL_GENES)
    data_corr['is_significant'] = (data_corr['p_value'] < 0.05)
    data_corr.set_index(['bcl_gene', 'gene'], append=True, inplace=True)

    return data_corr[['is_significant', 'R', 'p_value']]


def report_correlation_stats_between_bcl_family_and_genes(stats):
    """Create and style excel spreadsheet with statistical results for association between gene expression and cell_line.
    :param stats: pandas DataFrame, as returned by
                  compute_correlations_stats_between_bcl_family_and_genes(significant_genes)
    Export results to a styled excel spreadsheet.
    """
    # Pretty-up dataset names for export
    stats = stats.copy().reset_index('dataset')
    stats['dataset'] = stats.dataset.map({'ccle': 'CCLE', 'pdx': 'GAO', 'metabric': 'METABRIC'})
    stats.set_index('dataset', append=True, inplace=True)
    stats['Type'] = stats.index.get_level_values('dataset').map({
        'CCLE': 'Cell lines collection',
        'GAO': 'PDXs collection',
        'METABRIC': 'Patients collection',
    })

    # Format table for export
    stats['R'] = stats.R.round(2)
    stats['p_value'] = stats['p_value'].apply(lambda x: py_utils.format_pvalue.format_pvalue(x))

    stats = stats.join(utils.data.get_relevant_genes(), on='gene')
    stats.reset_index(inplace=True)
    stats['bcl_gene'] = stats.bcl_gene.astype('str')
    stats.loc[(stats.dataset == "TCGA") & (stats.bcl_gene == "BCL2L1"), 'bcl_gene'] = 'BCLXL'
    stats['dataset'] = pandas.Categorical(stats.dataset, ['CCLE', 'GAO', 'METABRIC'])
    stats['bcl_gene'] = pandas.Categorical(stats.bcl_gene, ['BCL2', 'BCL2L1', 'BCLXL'])
    stats.rename(columns={
        'dataset': 'Dataset',
        'gene_group': 'Gene group',
        'gene': 'Gene',
        'bcl_gene': 'BCL gene',
        'R': 'Spearman correlation R',
        'p_value': 'P-value',
    }, inplace=True)
    stats.set_index(['Dataset', 'Type', 'BCL gene', 'Gene group', 'Gene'], inplace=True)
    stats = stats[['Spearman correlation R', 'P-value']].sort_index()

    # Apply style and export to excel spreadsheet
    stats.style. \
        set_properties(**{'text-align': 'left'}). \
        set_properties(subset=['Spearman correlation R', 'P-value'], **{'text-align': 'right'}). \
        to_excel(pathlib.Path('outputs', 'supplementary_table_2.xlsx'), index=True)

    print('Results from correlation analysis saved in outputs/supplementary_table_2.xlsx')
