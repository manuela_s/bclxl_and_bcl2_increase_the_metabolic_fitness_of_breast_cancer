import pathlib
import pandas

COMPLEXES_ORDER = ['Complex I', 'Complex II', 'Complex III', 'Complex IV', 'Complex V']


def get_relevant_genes():
    """Get genes for respiratory chain complexes retrieved from the "gene names" database curated by the HGNC - HUGO
    Gene Nomenclature Committee (https://www.genenames.org/data/genegroup/#!/group/639).
    :return: pandas DataFrame, one row per 'gene' (index) and 'gene_group' as column.
    """
    genes = pandas.concat({
        'Complex I': pandas.read_csv(pathlib.Path('data', 'gene_groups', 'group-640.csv'), skiprows=1),
        'Complex II': pandas.read_csv(pathlib.Path('data', 'gene_groups', 'group-641.csv'), skiprows=1),
        'Complex III': pandas.read_csv(pathlib.Path('data', 'gene_groups', 'group-642.csv'), skiprows=1),
        'Complex IV': pandas.read_csv(pathlib.Path('data', 'gene_groups', 'group-643.csv'), skiprows=1),
        'Complex V': pandas.read_csv(pathlib.Path('data', 'gene_groups', 'group-644.csv'), skiprows=1),
    }, names=['gene_group'], axis=0)

    # Remap symbols to aliases used by mygene library
    genes.loc[:, 'symbol'] = genes['Approved symbol'].replace({
        'MT-CYB': 'CYTB',
        'MT-CO1': 'COX1',
        'MT-CO2': 'COX2',
        'MT-CO3': 'COX3',
        'MT-ATP6': 'ATP6',
        'MT-ATP8': 'ATP8',
        'MT-ND1': 'ND1',
        'MT-ND2': 'ND2',
        'MT-ND3': 'ND3',
        'MT-ND4': 'ND4',
        'MT-ND4L': 'ND4L',
        'MT-ND5': 'ND5',
        'MT-ND6': 'ND6',
    })

    genes = genes.reset_index()[['symbol', 'gene_group']]
    genes.loc[:, 'gene_group'] = pandas.Categorical(genes.gene_group, COMPLEXES_ORDER)
    genes.rename(columns={'symbol': 'gene'}, inplace=True)

    return genes.sort_values(by=['gene_group']).set_index('gene')


def _get_mcf7_data_helper():
    """Get MCF7 (GSE158808) transcriptomic data restricted to genes of interest.
    :return: pandas DataFrame, one row per sample with indices 'cell_line' and 'replica' and one column for gene as
             exported by 1_prepare_data.prepare_mcf7_data.
    """
    return pandas.read_csv(pathlib.Path('data', 'mcf7_clones', 'mcf7.csv'), index_col=['cell_line', 'replica']). \
        rename_axis(columns=['gene'])


def get_mcf7_data():
    """Get MCF7 (GSE158808) transcriptomic data restricted to genes of interest in a tall table format.
    :return: pandas DataFrame, one row per 'cell_line', 'replica', 'gene' and 'gene_group' (indices) and a column 'expression'
             with normalized values as returned by _get_mcf7_data_helper.
    """
    return _get_mcf7_data_helper().stack().to_frame('expression').join(get_relevant_genes(), how='inner'). \
        set_index('gene_group', append=True)


def get_ccle_data():
    """Get CCLE (Barretina et al., Nature, 2012; PMID: 22460905) transcriptomic data restricted to genes of interest.
    :return: pandas DataFrame, one row per sample with indices 'cell_line' and one column for gene as exported by
             1_prepare_data.prepare_ccle_data.
    """
    return pandas.read_csv(pathlib.Path('data', 'ccle', 'ccle.csv'), index_col='cell_line'). \
        rename_axis(columns=['gene'])


def get_pdx_data():
    """Get PDXs (Gao et al., Nature Medicine, 2015; PMID: 26479923) transcriptomic data restricted to genes of interest.
    :return: pandas DataFrame, one row per sample with indices 'pdx' and one column for gene as exported by
             1_prepare_data.prepare_pdx_data.
    """
    return pandas.read_csv(pathlib.Path('data', 'pdxs_gao', 'pdxs_gao.csv'), index_col='pdx'). \
        rename_axis(columns=['gene'])


def get_metabric_data():
    """Get METABRIC (Curtis et al., Nature, 2012; PMID: 22522925) transcriptomic data restricted to genes of interest.
    :return: pandas DataFrame, one row per sample with indices 'patient_id' and one column for gene as exported by
             1_prepare_data.prepare_metabric_data.
    """
    return pandas.read_csv(pathlib.Path('data', 'metabric', 'metabric.csv'), index_col='patient_id'). \
        rename_axis(columns=['gene'])
