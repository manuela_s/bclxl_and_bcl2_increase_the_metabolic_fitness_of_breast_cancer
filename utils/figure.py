import pathlib

import matplotlib.pyplot
import numpy
import pandas
import seaborn
import sklearn.preprocessing

COMPLEXES_ORDER = ['Complex I', 'Complex II', 'Complex III', 'Complex IV', 'Complex V']
DATASETS_ORDER = ['mcf7', 'ccle', 'pdx', 'metabric']
BIN_LABELS = ['0', '1', '2', '3', '4', '5', '6']


def _process_mcf7(mcf7):
    """Process MCF7 gene expression data and generate DataFrame with bin values.
    :param mcf7: pandas DataFrame, as returned by utils.data.get_mcf7_data();
    :return:
      - pandas DataFrame, indices 'gene' and 'gene_group' and columns for cell line/replicas and bins as values;
      - sequence of strings, labels for the 7 value bins for MCF7 gene expression;
    """
    mcf7 = mcf7.copy()

    # Z-score per gene:
    mcf7.loc[:, 'std_expression'] = mcf7.groupby(['gene_group', 'gene']).expression.transform(
        lambda x: sklearn.preprocessing.StandardScaler().fit_transform(x.values.reshape(-1, 1)).reshape(-1))

    # Bin z-scored values:
    mcf7_edges = [-numpy.Inf, -1.25, -0.75, -0.25, 0.25, 0.75, 1.25, numpy.Inf]
    mcf7.loc[:, 'std_expression_bin'] = pandas.cut(mcf7.std_expression, bins=mcf7_edges)
    mcf7_bin_labels = mcf7.std_expression_bin.cat.categories.astype('str').to_series()
    mcf7.loc[:, 'value'] = mcf7.std_expression_bin.cat.rename_categories(BIN_LABELS)

    # Construct column identifier from cell_line and replica:
    mcf7.loc[:, 'col'] = mcf7.index.get_level_values('cell_line').str.replace('MCF7_', '') + '_' + \
                         mcf7.index.get_level_values('replica').str.replace('replica_', 'R')

    mcf7_w = mcf7.reset_index(['replica', 'cell_line'], drop=True).set_index('col', append=True).value. \
        unstack()
    mcf7_w.columns = pandas.MultiIndex.from_product([['mcf7'], [True], mcf7_w.columns],
                                                    names=['dataset', 'is_significant', 'bcl_gene'])

    return mcf7_w, mcf7_bin_labels


def _process_validation_datasets(corr_data):
    """Process correlation statistics for validation datasets and generate DataFrame with bin values.
    :param corr_data: pandas DataFrame, as returned by
                      utils.stats.compute_correlations_stats_between_bcl_family_and_genes(significant_genes);
    :return:
      - pandas DataFrame, indices 'gene' and 'gene_group' and columns for bcl gene and bins as values;
      - sequence of strings, labels for the 7 value bins for the correlation statistics for the validation datasets.
    """
    corr_data = corr_data.copy()

    # Bin correlation values:
    corr_data_edges = [-numpy.Inf, -0.25, -0.15, -0.05, 0.05, 0.15, 0.25, numpy.Inf]
    corr_data.loc[:, 'R_bins'] = pandas.cut(corr_data.R, bins=corr_data_edges)
    corr_data_bin_labels = corr_data.R_bins.cat.categories.astype('str')
    corr_data.loc[:, 'value'] = corr_data.R_bins.cat.rename_categories(BIN_LABELS)

    corr_data.set_index('is_significant', append=True, inplace=True)
    corr_data_w = corr_data.value.unstack(['dataset', 'is_significant', 'bcl_gene'])

    return corr_data_w, corr_data_bin_labels


def _combine_mcf7_and_validation_datasets(mcf7, corr_data):
    """Combine gene profiles for MCF7 clones and correlation statistics for validation datasets into a single DataFrame.
    :param mcf7: pandas DataFrame, as returned by utils.data.get_mcf7_data();
    :param corr_data: pandas DataFrame, as returned by
                      utils.stats.compute_correlations_stats_between_bcl_family_and_genes();
    :return:
      - pandas DataFrame, with
        - indices:
          - 'dataset': either mcf7, ccle, pdx or metabric;
          - 'col': either 'cell_line' + 'replica' for mcf7 or bcl gene for ccle, pdx and metabric;
          - 'gene_group';
          - 'gene';
          - 'is_significant';
        - columns:
          - 'value': float, either relative expression for mcf7 or correlation for ccle, pdx and metabric, binned into 7
                     bins, 0:6;
      - sequence of strings, as returned by _process_mcf7();
      - sequence of strings, as returned by _process_validation_datasets().
    """
    # MCF7
    mcf7_w, mcf7_bin_labels = _process_mcf7(mcf7)

    # Validation datasets: CCLE, PDX and METABRIC
    corr_data_w, corr_data_bin_labels = _process_validation_datasets(corr_data)

    data = pandas.concat([mcf7_w.reset_index('gene_group'), corr_data_w], axis=1). \
        set_index('gene_group', append=True). \
        rename_axis(columns={'bcl_gene': 'col'}). \
        stack(['dataset', 'is_significant', 'col']). \
        to_frame('value')

    data['value'] = pandas.Categorical(data.value, BIN_LABELS, ordered=True)
    data.reset_index(inplace=True)
    data['gene_group'] = pandas.Categorical(data.gene_group, COMPLEXES_ORDER)
    data['dataset'] = pandas.Categorical(data.dataset, DATASETS_ORDER)
    data['col'] = pandas.Categorical(
        data.col,
        ['PSFFV_R1', 'PSFFV_R2', 'BCL2_R1', 'BCL2_R2', 'BCLXL_R1', 'BCLXL_R2', 'BCL2', 'BCL2L1'])
    data['is_significant'] = pandas.Categorical(data.is_significant, [True, False])
    data.set_index(['dataset', 'col', 'gene_group', 'gene', 'is_significant'], inplace=True)
    data.sort_index(inplace=True)

    return data, mcf7_bin_labels, corr_data_bin_labels


def _plot_panel_helper(data, color):
    """Helper for seaborn.FacetGrid.map_dataframe to plot each bubble plot panel.
    :param data: pandas DataFrame, including columns 'col', 'gene', 'is_significant' and 'value';
    :param color: unused.
    """
    ax = matplotlib.pyplot.gca()
    seaborn.scatterplot(
        x='col',
        y='gene',
        size='value',
        size_order=data.value.cat.categories,
        sizes=dict(zip(data.value.cat.categories, numpy.linspace(4, 40, 7))),
        hue='value',
        hue_order=data.value.cat.categories,
        palette=dict(zip(data.value.cat.categories, matplotlib.cm.RdYlBu_r._resample(7)(range(7)))),
        style='is_significant',
        style_order=[True, False],
        marker='o',
        edgecolor='k',
        linewidth=0.5,
        data=data,
        ax=ax,
    )
    x_max = max(ax.xaxis.get_units()._mapping.values())
    y_max = max(ax.yaxis.get_units()._mapping.values())
    matplotlib.pyplot.xlim(-0.5, x_max + 0.5)
    matplotlib.pyplot.ylim(-0.5, y_max + 0.5)
    dataset, = data.dataset.unique()
    if dataset == 'mcf7':
        ax.vlines([1.5, 3.5], ymin=-0.5, ymax=y_max + 0.5, linestyles='dotted')
    else:
        ax.vlines([0.5], ymin=-0.5, ymax=y_max + 0.5, linestyles='dotted')


def plot_association_between_bcl_family_and_genes(mcf7, corr_data):
    """Plot association between BCL family and genes found to be altered in MCF7 clones.
    Figure includes sub-panels organized in a grid where rows represent genes grouped by respiratory chain complex and
    columns represent datasets (MCF7 cell lines and validation sets).
    Each bubble plot panel shows values where markers are size- and color-coded based on:
    - leftmost panels (MCF7 clones): rlog normalized and z-scored gene expression levels for the control and BCL2- and
                                     BCL-xL overexpressing MCF7 clones (in duplicates, R1 and R2);
    - remaining panels: Spearman correlation between BCL2 or BCL-xL and each gene of interest for the validation
                        datasets ('ccle', 'pdx', 'metabric');
    :param mcf7: pandas DataFrame, as returned by utils.data.get_mcf7_data();
    :param corr_data: pandas DataFrame, as returned by
                      utils.stats.compute_correlations_stats_between_bcl_family_and_genes();
    Export pdf figure to outputs/figure8.pdf.
    """
    data, mcf7_bin_labels, corr_data_bin_labels = _combine_mcf7_and_validation_datasets(mcf7, corr_data)

    n_genes_per_gene_group = data.query('dataset == "mcf7"').groupby('gene_group').size() / \
                             data.query('dataset == "mcf7"').groupby('col').size().index.nunique()

    g = seaborn.FacetGrid(
        data.reset_index(),
        row='gene_group',
        row_order=COMPLEXES_ORDER,
        col='dataset',
        col_order=DATASETS_ORDER,
        height=1.2,
        aspect=2,
        sharex='col',
        sharey='row',
        gridspec_kws=dict(
            top=0.95,
            bottom=0.05,
            left=0.3,
            right=0.7,
            hspace=0.7,
            height_ratios=n_genes_per_gene_group,
            width_ratios=[6, 2, 2, 2],
        ),
        despine=False,
        margin_titles=True,
    )
    for row in g.axes:
        # Share yaxis unitdata between all axes in the row, so a gene maps to the same y-value:
        ud = matplotlib.category.UnitData()
        for ax in row:
            ax.yaxis.converter = matplotlib.category.StrCategoryConverter()
            ax.yaxis.set_units(ud)
    for col in g.axes.T:
        # Share xaxis unitdata between all axes in the column, so a cell line/gene maps to the same x-value:
        ud = matplotlib.category.UnitData()
        for ax in col:
            ax.xaxis.converter = matplotlib.category.StrCategoryConverter()
            ax.xaxis.set_units(ud)

    g.map_dataframe(_plot_panel_helper)

    # Workaround for https://github.com/mwaskom/seaborn/issues/2161
    for an in g.fig.findobj(matplotlib.pyplot.Annotation):
        an.remove()

    g.set_titles(row_template='{row_name}', col_template='{col_name}')
    g.set_xlabels('')
    g.set_ylabels('')

    # Replace xticklabels for MCF7 to distinguish between 'cell_line' and 'replica'
    # Verify that columns are in expected order (assert only works when figure is drawn).
    # assert [x.get_text() for x in g.axes[-1][0].get_xticklabels()] == [
    #     'PSFFV_R1', 'PSFFV_R2', 'BCL2_R1', 'BCL2_R2', 'BCLXL_R1', 'BCLXL_R2']
    g.axes[-1][0].set_xticklabels(['R1', 'R2', 'R1', 'R2', 'R1', 'R2'])
    for i, cell_line in enumerate(['PSFFV', 'BCL2', 'BCLXL']):
        g.axes[-1][0].text(0.5 + 2 * i, -3, cell_line, horizontalalignment='center', verticalalignment='top')

    # Add custom legends:
    bin_handles = list(x for x in matplotlib.legend._get_legend_handles(g.fig.axes, None) if x._label in BIN_LABELS)
    g.fig.legend(bin_handles, mcf7_bin_labels, title='Relative gene expression\n[z-score]', loc='upper right')
    g.fig.legend(bin_handles, corr_data_bin_labels, title='Spearman correlation [R-value]', loc='lower right')
    stats_handles = list(x for x in matplotlib.legend._get_legend_handles(g.fig.axes, None) if x._label in
                         ['True', 'False'])
    g.fig.legend(stats_handles, ['P<0.05', 'P>=0.05'], title='Statistical\nsignificance', loc='center right')

    matplotlib.pyplot.savefig(pathlib.Path('outputs', 'figure8.pdf'))
